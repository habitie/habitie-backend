import dotenv from 'dotenv';
import mongooseInitialize from './loaders/mongoose';
import serverInitialize from './loaders/express';
import apiInitialize from './loaders/api';

dotenv.config();

const PORT = parseInt(process.env.PORT);

const app = serverInitialize();
mongooseInitialize();
apiInitialize(app);

app.listen(PORT, process.env.IP, () => {
    console.log(`Server has started on port ${PORT}`);
});
