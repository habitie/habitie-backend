import * as path from 'path';
import { fileURLToPath } from 'url';
import methodOverride from 'method-override';
import express, { Express } from 'express';

const serverInitialize = (): Express => {
    const __dirname = path.dirname(fileURLToPath(import.meta.url));

    const app = express();

    app.use(express.json());
    app.use(
        express.urlencoded({
            extended: true
        })
    );
    app.use(express.static(path.join(__dirname, '/public')));
    app.use(methodOverride('_method'));

    return app;
};

export default serverInitialize;
