import indexRoutes from '../api/index';
import { Express } from 'express';

const apiInitialize = (app: Express): void => {
    app.use('/', indexRoutes);
};

export default apiInitialize;
