import mongoose from 'mongoose';

const mongooseInitialize = (): void => {
    const url = process.env.DATABASEURL.toString() || 'mongodb://localhost/yelp_camp';
    mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
};

export default mongooseInitialize;
