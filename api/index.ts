import express from 'express';

const router = express.Router();

//Root Route
router.get('/', (req, res) => {
    res.render('Landing Page');
});

export default router;
