# [Backend] habitie

To run application:

1. git clone git@bitbucket.org:habitie/habitie-backend.git
2. cd habitie
3. mongod (mongo demon)
4. node app.js

\*node.js and mongodb are required to run habitie backend.
